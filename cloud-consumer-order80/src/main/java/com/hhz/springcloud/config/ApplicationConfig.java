package com.hhz.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * 配置类
 *
 * @Author Rem
 * @Date 2020-03-09
 */

@Configuration
public class ApplicationConfig {


    /**
     * RestTemplate提供了多种便捷访问远程Http服务的方法，
     * httpClient升级版RestTemplate  进行更深的封装
     *
     * 是一种简单便捷的访问restful服务的模板类，是spring提供的用于访问Rest服务的客户端模板工具集。
     * @return
     */
    @Bean
    //开启负载均衡 默认轮询
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
