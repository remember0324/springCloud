package com.hhz.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author Rem
 * @Date 2020-03-14
 * <p>
 * 由于大型项目中可能存在大量的微服务，而每个微服务都有自己的配置文件（application.properties），导致存在以下问题：
 * 1、大量的配置文件分散在各个微服务的代码中，不易管理
 * 2、使用共同的配置时（例如数据库、redis），当共同的配置修改时，所有使用到的微服务的配置都需要修改
 * 3、配置内容的安全性以及权限无法保障
 * 4、配置更新后，相应的微服务需要重启
 * <p>
 * 在spring cloud中使用config组件来统一管理配置文件，来解决以上存在的问题。
 * spring cloud config的原理类似于eureka，有server端和client端。server端负责从远端或本地的配置地址获取到配置文件，而各个微服务（client端）则从server端获取相应的配置。
 */
/**
 * 由于大型项目中可能存在大量的微服务，而每个微服务都有自己的配置文件（application.properties），导致存在以下问题：
 * 1、大量的配置文件分散在各个微服务的代码中，不易管理
 * 2、使用共同的配置时（例如数据库、redis），当共同的配置修改时，所有使用到的微服务的配置都需要修改
 * 3、配置内容的安全性以及权限无法保障
 * 4、配置更新后，相应的微服务需要重启
 *
 * 在spring cloud中使用config组件来统一管理配置文件，来解决以上存在的问题。
 * spring cloud config的原理类似于eureka，有server端和client端。server端负责从远端或本地的配置地址获取到配置文件，而各个微服务（client端）则从server端获取相应的配置。
 */

/**
 * http://config-3344.com:3344/master/config-test.yml  访问带上分支
 * http://config-3344.com:3344/config-test.yml         访问不带上分支
 *
 * post请求全局刷新
 * http://localhost:3344/actuator/bus-refresh
 * post请求配置rabbitMQ单服务刷新
 * http://localhost:3344/actuator/bus-refresh/config-client3355:3355
 */
@SpringBootApplication
@EnableConfigServer

@EnableEurekaClient
public class ConfigCenter3344 {

    public static void main(String[] args) {
        SpringApplication.run(ConfigCenter3344.class, args);
    }
}
