package com.hhz.springcloud.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author Rem
 * @Date 2020-03-16
 * @Version 1.0
 */

@SpringBootApplication
@EnableDiscoveryClient
public class ProviderPayment9002 {

    public static void main(String[] args) {
        SpringApplication.run(ProviderPayment9002.class, args);
    }
}
