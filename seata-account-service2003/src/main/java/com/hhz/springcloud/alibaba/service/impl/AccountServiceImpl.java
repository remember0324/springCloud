package com.hhz.springcloud.alibaba.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhz.springcloud.alibaba.mapper.AccountMapper;
import com.hhz.springcloud.alibaba.pojo.Account;
import com.hhz.springcloud.alibaba.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @Author Rem
 * @Date 2020-03-19
 */

@Service
@Slf4j
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {


    /**
     * 修改账户
     * SELECT id,total,user_id,residue,userd FROM t_account WHERE (user_id = ?)
     * UPDATE t_account SET userd=?,residue=? WHERE (user_id = ?)
     *
     * @param userId
     * @param money
     */
    @Override
    public void decrease(Long userId, BigDecimal money) {

        UpdateWrapper<Account> wrapper = new UpdateWrapper<>();

        wrapper.eq("user_id", userId);
        Account account = baseMapper.selectOne(wrapper);

        wrapper.set("userd", account.getUserd().add(money));
        wrapper.set("residue", account.getResidue().subtract(money));

        baseMapper.update(null, wrapper);
    }
}

