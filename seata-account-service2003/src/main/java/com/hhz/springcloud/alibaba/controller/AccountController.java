package com.hhz.springcloud.alibaba.controller;

import com.hhz.springcloud.alibaba.service.AccountService;
import com.hhz.springcloud.entites.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/**
 * @Author Rem
 * @Date 2020-03-21
 */

@Slf4j
@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/storage/account")
    CommonResult decreaseAccount(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money) {

        try {
            //测试 休眠20s
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        accountService.decrease(userId, money);
        return new CommonResult(200, "账户扣减成功");
    }
}
