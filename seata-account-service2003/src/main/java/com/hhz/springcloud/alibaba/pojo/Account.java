package com.hhz.springcloud.alibaba.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author Rem
 * @Date 2020-03-19
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_account")
public class Account implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private BigDecimal total;

    private BigDecimal userd;

    private BigDecimal residue;

}
