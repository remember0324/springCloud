package com.hhz.springcloud.alibaba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhz.springcloud.alibaba.pojo.Account;
import org.springframework.stereotype.Repository;

/**
 * @Author Rem
 * @Date 2020-03-19
 */

@Repository
public interface AccountMapper extends BaseMapper<Account> {

}
