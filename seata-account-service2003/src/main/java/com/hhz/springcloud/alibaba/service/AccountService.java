package com.hhz.springcloud.alibaba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hhz.springcloud.alibaba.pojo.Account;

import java.math.BigDecimal;

/**
 * @Author Rem
 * @Date 2020-03-19
 */

public interface AccountService extends IService<Account> {

    void decrease(Long userId, BigDecimal money);

}
