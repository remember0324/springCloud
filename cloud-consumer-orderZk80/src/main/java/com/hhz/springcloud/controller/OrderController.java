package com.hhz.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @Author Rem
 * @Date 2020-03-11
 * @Version 1.0
 */

@RestController
public class OrderController {

    //http://localhost:8004/payment/zk
    //地址是区分大小写的
    private final static String INVOKE_URL = "http://zk-payment-service";

    @Resource
    private RestTemplate restTemplate;

    /**
     * http://localhost/order/zk
     * @return
     */
    @GetMapping("/order/zk")
    public String getOrder() {
        return restTemplate.getForObject(INVOKE_URL + "/payment/zk", String.class);
    }
}
