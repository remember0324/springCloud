package com.hhz.springcloud.alibaba.service.openFeign;

import com.hhz.springcloud.entites.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * 调用账户
 *
 * @Author Rem
 * @Date 2020-03-20
 */

@Component
@FeignClient("seata-account-service")
public interface AcccountService {

    /**
     * 扣减money
     *
     * @param productId
     * @param count
     * @return
     */
    @PostMapping("/storage/account")
    CommonResult decreaseAccount(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money);
}
