package com.hhz.springcloud.alibaba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhz.springcloud.alibaba.pojo.Order;
import org.springframework.stereotype.Repository;

/**
 * @Author Rem
 * @Date 2020-03-19
 */

@Repository
public interface OrderMapper extends BaseMapper<Order> {

}
