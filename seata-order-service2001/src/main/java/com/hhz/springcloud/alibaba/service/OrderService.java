package com.hhz.springcloud.alibaba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hhz.springcloud.alibaba.pojo.Order;

/**
 * @Author Rem
 * @Date 2020-03-19
 */

public interface OrderService extends IService<Order> {

    Integer addOrder(Order order);

    Integer updateOrder(Long userId, int status);

}
