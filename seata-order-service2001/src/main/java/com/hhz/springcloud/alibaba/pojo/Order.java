package com.hhz.springcloud.alibaba.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author Rem
 * @Date 2020-03-19
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_order")
public class Order implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    @TableField(value = "user_id")//mybatis会自动将下划线转大写 不用注解也可以映射
    private Long userId;

    private Long productId;

    private Integer count;

    private BigDecimal money;

    private int status;

}
