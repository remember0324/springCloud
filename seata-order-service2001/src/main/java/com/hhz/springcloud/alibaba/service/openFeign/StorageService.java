package com.hhz.springcloud.alibaba.service.openFeign;

import com.hhz.springcloud.entites.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 调用库存
 *
 * @Author Rem
 * @Date 2020-03-20
 */

@Component
@FeignClient(value = "seata-storage-service")
public interface StorageService {


    /**
     * 扣减 count 库存
     *
     * @param productId
     * @param count
     * @return
     */
    @PostMapping("/storage/decrease")
    CommonResult decreaseStorage(@RequestParam("productId") Long productId, @RequestParam("count") Integer count);


}
