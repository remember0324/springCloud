package com.hhz.springcloud.alibaba.controller;

import com.hhz.springcloud.alibaba.pojo.Order;
import com.hhz.springcloud.alibaba.service.OrderService;
import com.hhz.springcloud.entites.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Rem
 * @Date 2020-03-21
 */

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/order/create")
    public CommonResult create(@RequestBody  Order order) {
        Integer result = orderService.addOrder(order);
        return new CommonResult(200, "订单创建成功", result);
    }


}
