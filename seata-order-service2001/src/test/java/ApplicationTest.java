import com.hhz.springcloud.alibaba.SeataOrderService2001;
import com.hhz.springcloud.alibaba.pojo.Order;
import com.hhz.springcloud.alibaba.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

/**
 * @Author Rem
 * @Date 2020-03-08
 */


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeataOrderService2001.class)
public class ApplicationTest {

    @Autowired
    OrderService orderService;

    @Test
    public void testSelect() {
        Order order = new Order();
        order.setCount(4);
        order.setMoney(new BigDecimal(5));
        order.setProductId(2L);
        order.setUserId(2L);
        Integer integer = orderService.addOrder(order);

    }

    @Test
    public void testSelect2() {
        Integer integer = orderService.updateOrder(2L, 1);
        System.err.println(integer);
    }



}
