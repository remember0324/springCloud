package com.hhz.springcloud.controller;

import com.hhz.springcloud.servicre.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Rem
 * @Date 2020-03-12
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/payment/hystrix")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    /**
     * 正常情况下访问
     * http://localhost:8001/payment/hystrix/ok/{id}
     *
     * @param id
     * @return
     */
    @GetMapping("/ok/{id}")
    public String paymentInfo_OK(@PathVariable Long id) {
        log.info("ok:" + id);
        return paymentService.paymentInfo_OK(id);
    }


    /**
     * 超时测试
     * http://localhost:8001/payment/hystrix/timeout/{id}
     *
     * @param id
     * @return
     */
    @GetMapping("/timeout/{id}")
    public String paymentInfo_Timeout(@PathVariable Long id) {
        log.info("timeout:" + id);
        return paymentService.paymentInfo_Timeout(id);
    }

    /**
     * 熔断测试
     *
     * @param id
     * @return
     */
    @GetMapping("/circuit/{id}")
    public String paymentCircuitBreaker(@PathVariable Integer id) {
        log.info("服务熔断~~:" + id);
        return paymentService.paymentCircuitBreaker(id);
    }


}
