package com.hhz.springcloud.servicre;

import cn.hutool.core.util.IdUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;

/**
 * @Author Rem
 * @Date 2020-03-12
 * @Version 1.0
 */

@Service
public class PaymentService {

    //服务降级
    public String paymentInfo_OK(Long id) {
        return "线程池:" + Thread.currentThread().getName() + " id:" + id;
    }


    /**
     * 超时测试
     * sleep interrupted
     * HystrixCommand:一旦调用服务方法失败并抛出了错误信息后,会自动调用@HystrixCommand标注好的fallbckMethod调用类中的指定方法
     * execution.isolation.thread.timeoutInMilliseconds:线程超时时间3秒钟
     *
     * @param i d
     * @return
     */
    //设置超时服务降级的兜底方法 超过3s就进入  程序报错也会进入
    @HystrixCommand(fallbackMethod = "payment_TimeOutHandler", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
    })
    public String paymentInfo_Timeout(Long id) {
        // int a=10/0;
        int timeNum = 5000;
        try {
            Thread.sleep(timeNum);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "线程池:" + Thread.currentThread().getName() + " id:" + id + "耗时(s):" + timeNum;
    }

    //处理服务降级
    public String payment_TimeOutHandler(Long id) {
        return "超时兜底_线程池:" + Thread.currentThread().getName() + " id:" + id;
    }


    /**
     * 服务熔断
     * 服务进入熔断之后一段时间在如果在符合条件的范围内有请求过来会慢慢打开(半熔断)进入连接状态再全部恢复
     * 服务降级-》进而熔断-》恢复调用链路
     * <p>
     * <p>
     * 1、circuitBreaker.sleepWindowInMilliseconds
     * 断路器的快照时间窗，也叫做窗口期。可以理解为一个触发断路器的周期时间值，默认为10秒（10000）。
     * 2、circuitBreaker.requestVolumeThreshold
     * 断路器的窗口期内触发断路的请求阈值，默认为20。换句话说，假如某个窗口期内的请求总数都不到该配置值，那么断路器连发生的资格都没有。断路器在该窗口期内将不会被打开。
     * 3、circuitBreaker.errorThresholdPercentage
     * 断路器的窗口期内能够容忍的错误百分比阈值，默认为50（也就是说默认容忍 50%的错误率）。打个比方，假如一个窗口期内，发生了100次服务请求，其中50次出现了错误。在这样的情况下，断路器将会被打开。在该窗口期结束之前，即使第51次请求没有发生异常，也将被执行fallback逻辑。
     *
     * @param id
     * @return
     */
    @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_Fallbak", commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),// 是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),// 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"),// 时间窗口期/时间范文
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60")// 失败率达到多少后跳闸
    })
    public String paymentCircuitBreaker(Integer id) {
        if (id < 0) {
            throw new RuntimeException("传入id不能小于0");
        }
        //生成一个随机数
        String simpleUUID = IdUtil.simpleUUID();
        return Thread.currentThread().getName() + "\t" + "调用成功，流水号：" + simpleUUID;
    }


    //处理服务熔断
    public String paymentCircuitBreaker_Fallbak(Integer id) {
        return "id不能为负数:" + Thread.currentThread().getName() + " id:" + id;
    }


}
