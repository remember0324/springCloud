package com.hhz.springcloud.alibaba.service;

import com.hhz.springcloud.entites.CommonResult;
import com.hhz.springcloud.entites.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Author Rem
 * @Date 2020-03-19
 * @Version 1.0
 */

@Service
@FeignClient(value = "nacos-payment-provider", fallback = OrderFallbackService.class)
public interface PaymentService {

    @GetMapping(value = "/paymentSQL/{id}")
    CommonResult<Payment> paymentSQL(@PathVariable("id") Long id);


}
