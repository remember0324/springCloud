package com.hhz.springcloud.alibaba.controller;

import com.hhz.springcloud.alibaba.service.PaymentService;
import com.hhz.springcloud.entites.CommonResult;
import com.hhz.springcloud.entites.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Rem
 * @Date 2020-03-19
 * @Version 1.0
 */

@RestController
public class TestOpenFeignController {

    @Autowired
    PaymentService paymentService;


    @GetMapping(value = "/consumer/payment/{id}")
    CommonResult<Payment> paymentSQL(@PathVariable("id") Long id) {
        return paymentService.paymentSQL(id);
    }


}
