package com.hhz.springcloud.alibaba.service;

import com.hhz.springcloud.entites.CommonResult;
import com.hhz.springcloud.entites.Payment;
import org.springframework.stereotype.Component;

/**
 * @Author Rem
 * @Date 2020-03-19
 * @Version 1.0
 */

@Component
public class OrderFallbackService implements PaymentService {


    @Override
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<>(449, "openFeign 异常处理", new Payment(id, null));
    }
}
