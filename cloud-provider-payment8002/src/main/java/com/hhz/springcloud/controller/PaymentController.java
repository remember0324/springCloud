package com.hhz.springcloud.controller;

import com.hhz.springcloud.entites.CommonResult;
import com.hhz.springcloud.entites.Payment;
import com.hhz.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * @Author Rem
 * @Date 2020-03-09
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;


    @PostMapping("/insert")
    public CommonResult insert(@RequestBody Payment payment) {
        Integer result = paymentService.insert(payment);
        if (result > 0) {
            log.info("插入数据库成功");
            return new CommonResult(200, "插入数据库成功  serverPort:" + serverPort, result);
        } else {
            log.info("插入数据库失败");
            return new CommonResult(444, "插入数据库失败");
        }
    }


    @GetMapping("/findById/{id}")
    public CommonResult<Payment> findById(@PathVariable("id") Long id) {
        Payment result = paymentService.findById(id);
        if (result != null) {
            log.info("查询成功");
            return new CommonResult(200, "查询成功 serverPort:" + serverPort, result);
        } else {
            log.info("查询失败");
            return new CommonResult(444, "查询失败");
        }
    }

    /**
     * 本服务的端口 测试自己写的轮询规则
     *
     * @return
     */
    @GetMapping("/serverPort")
    public String getServerPort() {
        return this.serverPort;
    }
}
