import com.hhz.springcloud.ProviderPayment8001;
import com.hhz.springcloud.entites.Payment;
import com.hhz.springcloud.mapper.PaymentMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Author Rem
 * @Date 2020-03-08
 */


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProviderPayment8001.class)
public class ApplicationTest {

    @Autowired
    private PaymentMapper paymentMapper;

    /**
     * 查询所有
     */
    @Test
    public void testSelect() {
        List<Payment> list = paymentMapper.selectList(null);
        list.forEach(System.err::println);
    }



}
