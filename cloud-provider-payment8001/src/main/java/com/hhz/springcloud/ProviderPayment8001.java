package com.hhz.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author Rem
 * @Date 2020-03-08
 */

@SpringBootApplication
@MapperScan("com.hhz.springcloud.mapper")
@EnableEurekaClient
@EnableDiscoveryClient
public class ProviderPayment8001 {

    public static void main(String[] args) {
        SpringApplication.run(ProviderPayment8001.class,args);
    }
}
