package com.hhz.springcloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhz.springcloud.entites.Payment;
import com.hhz.springcloud.mapper.PaymentMapper;
import com.hhz.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author Rem
 * @Date 2020-03-09
 * @Version 1.0
 */

@Transactional
@Service
public class PaymentServiceImpl extends ServiceImpl<PaymentMapper, Payment> implements PaymentService {


    //新增交易
    @Override
    public Integer insert(Payment payment) {
        return baseMapper.insert(payment);
    }

    //查找一个交易
    @Override
    public Payment findById(Long id) {
        return baseMapper.selectById(id);
    }
}
