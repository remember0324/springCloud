package com.hhz.springcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hhz.springcloud.entites.Payment;

/**
 * @Author Rem
 * @Date 2020-03-09
 * @Version 1.0
 */

public interface PaymentService extends IService<Payment> {

    Integer insert(Payment payment);

    Payment findById(Long id);

}
