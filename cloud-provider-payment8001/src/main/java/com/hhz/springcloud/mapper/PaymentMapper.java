package com.hhz.springcloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhz.springcloud.entites.Payment;
import org.springframework.stereotype.Repository;

/**
 * @Author Rem
 * @Date 2020-03-08
 */

@Repository
public interface PaymentMapper extends BaseMapper<Payment> {
}
