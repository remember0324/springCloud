package com.hhz.springcloud.controller;

import com.hhz.springcloud.entites.CommonResult;
import com.hhz.springcloud.entites.Payment;
import com.hhz.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author Rem
 * @Date 2020-03-09
 * @Version 1.0
 */

@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @Autowired
    private DiscoveryClient discoveryClient;


    /**
     * 新增订单
     * http://localhost:8001/payment/insert
     *
     * @param payment
     * @return
     */
    @PostMapping("/insert")
    public CommonResult insert(@RequestBody Payment payment) {
        Integer result = paymentService.insert(payment);
        if (result > 0) {
            log.info("插入数据库成功");
            return new CommonResult(200, "插入数据库成功  serverPort:" + serverPort, result);
        } else {
            log.info("插入数据库失败");
            return new CommonResult(444, "插入数据库失败");
        }
    }

    /**
     * 查询订单
     * http://localhost:8001/payment/findById/1
     *
     * @param id
     * @return
     */
    @GetMapping("/findById/{id}")
    public CommonResult<Payment> findById(@PathVariable("id") Long id) {
        Payment result = paymentService.findById(id);
        if (result != null) {
            log.info("查询成功");
            return new CommonResult(200, "查询成功 serverPort:" + serverPort, result);
        } else {
            log.info("查询失败");
            return new CommonResult(444, "查询失败");
        }
    }

    /**
     * 服务发现
     * http://localhost:8001/payment/getDiscover
     *
     * @return
     */
    @GetMapping("/getDiscover")
    public Object getDiscover() {
        List<String> services = discoveryClient.getServices();
        services.forEach(s -> log.info("服务名称:" + s));

        //一个服务下的所有实例   打印实例的情况
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance : instances) {
            log.info(instance.getServiceId() + "\t" + instance.getHost() + "\t" + instance.getPort() + instance.getUri());
        }
        return this.discoveryClient;
    }

    /**
     * 本服务的端口 测试自己写的轮询规则
     * http://localhost:8001/payment/serverPort
     *
     * @return
     */
    @GetMapping("/serverPort")
    public String getServerPort() {
        return this.serverPort;
    }

    /**
     * 模拟测试feign连接超时 默认时长1s
     * http://localhost:8001/payment/feign/timeout
     *
     * @return
     */
    @GetMapping("/feign/timeout")
    public String paymentFeignTimeout() {
        //等待3秒
        /*try {
        //老版
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return this.serverPort;
    }


    @GetMapping("/zipkin")
    public String paymentZipkin() {
        return "hi ,i'am paymentzipkin server fall back，welcome to atguigu，O(∩_∩)O哈哈~";
    }
}
