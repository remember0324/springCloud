package com.hhz.springcloud.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 通用返回类
 *
 * @Author Rem
 * @Date 2020-03-08
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResult<T> implements Serializable {

    private Integer code;
    private String message;
    private T dada;

    public CommonResult(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
