package com.hhz.springcloud.service;

/**
 * @Author Rem
 * @Date 2020-03-12
 */

import org.springframework.stereotype.Component;

/**
 * 8001微服务全服务兜底超时，异常，宕机解决方法
 */
@Component
public class OrderFallbackService implements PaymentHystrixService {


    @Override
    public String paymentInfo_OK(Long id) {
        return "OrderFallbackService----paymentInfo_OK--方法处理~~~";
    }

    @Override
    public String paymentInfo_Timeout(Long id) {
        return "OrderFallbackService----paymentInfo_Timeout--方法处理~~~";
    }
}
