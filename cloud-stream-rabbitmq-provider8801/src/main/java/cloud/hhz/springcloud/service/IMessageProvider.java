package cloud.hhz.springcloud.service;

/**
 * @Author Rem
 * @Date 2020-03-14
 */
public interface IMessageProvider {

    String send();
}
