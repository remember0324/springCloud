package cloud.hhz.springcloud.service.impl;

import cloud.hhz.springcloud.service.IMessageProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import java.util.UUID;


/**
 * @Author Rem
 * @Date 2020-03-14
 */

@EnableBinding(Source.class)//定义消息的推送管道
public class IMessageProviderImpl implements IMessageProvider {

    // 消息发送管道
    @Autowired
    private MessageChannel output;


    @Override
    public String send()
    {
        //发送数据
        String serial = UUID.randomUUID().toString();
        //开始发送
        output.send(MessageBuilder.withPayload(serial).build());

        //控制台打印
        System.out.println("*****serial: "+serial);
        //只是在页面打印
        return serial;
    }
}
