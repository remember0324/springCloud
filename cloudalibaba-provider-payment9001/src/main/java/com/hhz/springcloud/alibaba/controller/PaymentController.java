package com.hhz.springcloud.alibaba.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Rem
 * @Date 2020-03-16
 * @Version 1.0
 */

@RestController
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;


    @GetMapping("/payment/serverPort/{id}")
    public String getServerPort(@PathVariable("id") Integer id) {
        return "from nacos provider:" + serverPort + "\t id:" + id;
    }

}
