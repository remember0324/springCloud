package com.hhz.springcloud.alibaba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Author Rem
 * @Date 2020-03-16
 * @Version 1.0
 */

@RestController
public class PaymentOrderController {

    @Value("${service-url.nacos-user-service}")
    private String serviceUrl;

    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("/consumer/order/payment/{id}")
    public String getServerPort(@PathVariable("id") Integer id) {

        return restTemplate.getForObject(serviceUrl + "/payment/serverPort/" + id, String.class);
    }

}
