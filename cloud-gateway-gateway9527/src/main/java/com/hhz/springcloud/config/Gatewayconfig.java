package com.hhz.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 网关配置
 *
 * @Author Rem
 * @Date 2020-03-13
 * @Version 1.0
 */

@Configuration
public class Gatewayconfig {


    /**
     * 配置了一个id为route-name的路由规则
     * 当访问localhost:9527/guonei的时候，将会转发至https://news.baidu.com/guonei
     *
     * @param routeLocatorBuilder
     * @return
     */
    @Bean
    public RouteLocator customerLocator(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        routes.route("path_route_baidu", r -> r.path("/guonei")
                .uri("https://news.baidu.com/guonei")
        );
        return routes.build();
    }


    @Bean
    public RouteLocator customerLocator2(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        return routes.route("path_route_baidu2", r -> r.path("/guoji")
                .uri("https://news.baidu.com/guoji")
        ).build();
    }
}
