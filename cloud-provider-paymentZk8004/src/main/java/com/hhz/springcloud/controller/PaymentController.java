package com.hhz.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @Author Rem
 * @Date 2020-03-11
 * @Version 1.0
 */

@RestController
public class PaymentController {

    /**
     * http://localhost:8004/payment/zk
     *
     * @return
     */
    @GetMapping("/payment/zk")
    public String paymentZk() {
        return "Spring Cloud with zookeeper" + "\t" + UUID.randomUUID().toString();
    }


}
