package com.hhz.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author Rem
 * @Date 2020-03-11
 * @Version 1.0
 * {
 * 	"name": "cloud-payment-service",
 * 	"id": "2c41f1fc-5ff9-48a1-9cfa-e27631afb4d7",
 * 	"add
 * 	ress ":"
 * 	C64 - 20190729 WGL ","
 * 	port ":8004,"
 * 	sslPort ":null,"
 * 	payload ":{"
 * 	@class ":"
 * 	org.spri
 * 	ngframework.cloud.zookeeper.discovery.ZookeeperInstance ","
 * 	id ":"
 * 	application - 1 ","
 * 	n
 * 	ame ":"
 * 	cloud - payment - service ","
 * 	metadata ":{}},"
 * 	registrationTimeUTC ":1583894416918,
 * 	"serviceType": "DYNAMIC",
 * 	"uriSpec": {
 * 		"parts": [{
 * 			"value": "scheme",
 * 			"variable": true
 * 		}, {
 * 			"value": "://",
 * 			"variable": false
 * 		}, {
 * 			"value": "address",
 * 			"variable": true
 * 		}, {
 * 			"value": ":",
 * 			"variable": false
 * 		}, {
 * 			"value": "port",
 * 			"variable": true
 * 		}]
 * 	}
 * }
 */

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
public class PorviderPaymentZk8004 {

    public static void main(String[] args) {
        SpringApplication.run(PorviderPaymentZk8004.class,args);
    }
}
