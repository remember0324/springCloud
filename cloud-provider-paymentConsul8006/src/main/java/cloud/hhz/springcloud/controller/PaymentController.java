package cloud.hhz.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @Author Rem
 * @Date 2020-03-11
 * @Version 1.0
 */

@RestController
public class PaymentController {

    /**
     * http://localhost:8006/payment/consul
     *
     * @return
     */
    @GetMapping("/payment/consul")
    public String paymentZk() {
        return "Spring Cloud with consul" + "\t" + UUID.randomUUID().toString();
    }


}
