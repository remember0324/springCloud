package com.hhz.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Rem
 * @Date 2020-03-14
 */

@RestController
@RefreshScope   //配置文件自动刷新
public class ClientController {

    @Value("${config.info}")
    private String info;


    @GetMapping("/client/info")
    public String getInfo() {
        return info;
    }

}
