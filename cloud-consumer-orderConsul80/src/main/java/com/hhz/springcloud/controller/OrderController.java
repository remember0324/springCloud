package com.hhz.springcloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @Author Rem
 * @Date 2020-03-11
 * @Version 1.0
 */

@RestController
public class OrderController {

    private final static String INVOKE_URL = "http://consul-payment-service";

    @Resource
    private RestTemplate restTemplate;

    /**
     * http://localhost/order/consul
     *
     * @return
     */
    @GetMapping("/order/consul")
    public String getOrder() {
        return restTemplate.getForObject(INVOKE_URL + "/payment/consul", String.class);
    }
}
