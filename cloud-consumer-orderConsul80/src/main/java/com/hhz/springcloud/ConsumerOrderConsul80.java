package com.hhz.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author Rem
 * @Date 2020-03-11
 * @Version 1.0
 */

/**
 * Consul是一个服务网格（微服务间的 TCP/IP，负责服务之间的网络调用、限流、熔断和监控）解决方案，
 * 它是一个一个分布式的，高度可用的系统，而且开发使用都很简便。它提供了一个功能齐全的控制平面，
 * 主要特点是：服务发现、健康检查、键值存储、安全服务通信、多数据中心。
 *
 * 与其它分布式服务注册与发现的方案相比，Consul 的方案更“一站式”——内置了服务注册与发现框架、
 * 分布一致性协议实现、健康检查、Key/Value 存储、多数据中心方案，不再需要依赖其它工具。
 * Consul 本身使用 go 语言开发，具有跨平台、运行高效等特点，也非常方便和 Docker 配合使用。
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
public class ConsumerOrderConsul80 {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerOrderConsul80.class, args);
    }
}
