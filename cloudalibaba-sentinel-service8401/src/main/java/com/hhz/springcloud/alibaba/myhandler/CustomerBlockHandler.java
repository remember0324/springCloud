package com.hhz.springcloud.alibaba.myhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.hhz.springcloud.entites.CommonResult;

/**
 * @Author: Rem
 * @CreateDate: 2020/3/18 16:51
 * @Version: 1.0
 */
public class CustomerBlockHandler {
    public static CommonResult handlerException(BlockException exception) {
        return new CommonResult(4444, "按客戶自定义,global handlerException----1");
    }

    public static CommonResult handlerException2(BlockException exception) {
        return new CommonResult(4445, "按客戶自定义,global handlerException----2");
    }
}
