package com.hhz.springcloud.controller;

import com.hhz.springcloud.entites.CommonResult;
import com.hhz.springcloud.entites.Payment;
import com.hhz.springcloud.service.PaymentFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单调用类
 *
 * @Author Rem
 * @Date 2020-03-09
 */

@Slf4j
@RestController
@RequestMapping("consumer")
public class OrderController {

    @Autowired
    private PaymentFeignService paymentFeignService;

    @GetMapping("/insert")
    public CommonResult insert(Payment payment) {
        log.info("插入数据库成功");
        return paymentFeignService.insert(payment);
    }

    @GetMapping("/findById/{id}")
    public CommonResult<Payment> findById(@PathVariable Long id) {
        log.info("查询成功");
        return paymentFeignService.findById(id);
    }

    @GetMapping("/timeout")
    public String testTimeout() {
        log.info("测试连接");
        return paymentFeignService.paymentFeignTimeout();
    }

}
