package com.hhz.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * OpenFeign
 * 内部集成了ribbon
 *
 * @Author Rem
 * @Date 2020-03-10
 * @Version 1.0
 */

/**
 * Feign是一个声明式的Web Service客户端。它的出现使开发Web Service客户端变得很简单。
 * 使用Feign只需要创建一个接口加上对应的注解，比如：FeignClient注解。Feign有可插拔的注解，
 * 包括Feign注解和JAX-RS注解。Feign也支持编码器和解码器，Spring Cloud Open Feign对Feign进行增强支持Spring MVC注解，可以像Spring Web一样使用HttpMessageConverters等。
 * <p>
 * Feign是一种声明式、模板化的HTTP客户端。在Spring Cloud中使用Feign，
 * 可以做到使用HTTP请求访问远程服务，就像调用本地方法一样的，开发者完全感知不到这是在调用远程方法，更感知不到在访问HTTP请求。
 * <p>
 * 功能可插拔的注解支持，包括Feign注解和JAX-RS注解
 * 。支持可插拔的HTTP编码器和解码器（Gson，Jackson，Sax，JAXB，JAX-RS，SOAP）。
 * 支持Hystrix和它的Fallback。支持Ribbon的负载均衡。支持HTTP请求和响应的压缩。
 * 灵活的配置：基于 name 粒度进行配置支持多种客户端：JDK URLConnection、apache httpclient、okhttp，ribbon）支持日志支持错误重试url支持占位符可以不依赖注册中心独立运行
 */

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient
@EnableFeignClients
public class ConsumerFeignOrder80 {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerFeignOrder80.class, args);
    }

}
