package com.hhz.springcloud.comfig;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: Rem
 * @CreateDate: 2020/3/12 10:06
 * @Version: 1.0
 */
@Configuration
public class FeignConfig {

    /**
     * 设置feignClient的日志级别
     *
     * @return
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        // 请求和响应的头信息,请求和响应的正文及元数据
        return Logger.Level.FULL;
    }
}