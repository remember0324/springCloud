package com.hhz.springcloud.service;

import com.hhz.springcloud.entites.CommonResult;
import com.hhz.springcloud.entites.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @Author Rem
 * @Date 2020-03-11
 */

@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService {

    /**
     * feign的注解路径一定与被调用的controller一致 不能忘记头部的requestMapping
     * 在@PathVariable("id")  不能用@Parm("id")代替   ("id")即使名称和路径上一样 在feign中也不可省略
     * post 请求中@RequestBody可以省略
     *
     * @param id
     * @return
     */

    @GetMapping("/payment/findById/{id}")
    CommonResult<Payment> findById(@PathVariable("id") Long id);

    @PostMapping("/payment/insert")
    CommonResult insert(Payment payment);

    //模拟测试超时
    @GetMapping("/payment/feign/timeout")
    String paymentFeignTimeout();
}
