package com.hhz.springcloud.alibaba.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhz.springcloud.alibaba.mapper.StorageMapper;
import com.hhz.springcloud.alibaba.pojo.Storage;
import com.hhz.springcloud.alibaba.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @Author Rem
 * @Date 2020-03-19
 */

@Service
@Slf4j
public class StorageServiceImpl extends ServiceImpl<StorageMapper, Storage> implements StorageService {

    /**
     * 修改库存
     * UPDATE t_storage SET used = used +10, residue= residue-10 where product_id= 1
     *
     * @param productId
     * @param count
     */

    @Override
    public void decrease(Long productId, Integer count) {
        UpdateWrapper<Storage> wrapper = new UpdateWrapper<>();

        wrapper.setSql("used = used +" + count + ", residue= residue-" + count + " where product_id= " + productId);

        update(wrapper);

    }
}

