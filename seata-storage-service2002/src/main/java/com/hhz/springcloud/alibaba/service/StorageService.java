package com.hhz.springcloud.alibaba.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.hhz.springcloud.alibaba.pojo.Storage;

/**
 * @Author Rem
 * @Date 2020-03-19
 */

public interface StorageService extends IService<Storage> {

    void decrease(Long productId, Integer count);
}
