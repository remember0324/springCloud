package com.hhz.springcloud.alibaba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhz.springcloud.alibaba.pojo.Storage;
import org.springframework.stereotype.Repository;

/**
 * @Author Rem
 * @Date 2020-03-19
 */

@Repository
public interface StorageMapper extends BaseMapper<Storage> {

}
